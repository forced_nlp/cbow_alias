import os

from commands.paths import TOKENIZED_CORPUS_PATH
from model.dictionary_builder import DictionaryBuilder
from model.dictionary_file_manager import DictionaryFileManager


def cmd():
    corpus_file_path = os.path.join(TOKENIZED_CORPUS_PATH, 'en', 'tokenized_corpus.txt')
    lemmas_file_path = os.path.join(TOKENIZED_CORPUS_PATH, 'en', 'lemmatized_corpus.txt')

    builder = DictionaryBuilder()
    dc = builder.build('en', corpus_file_path, lemmas_file_path)

    mgr = DictionaryFileManager()
    mgr.save(dc)


if __name__ == '__main__':
    cmd()
    print('Finished')
