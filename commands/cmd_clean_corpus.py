import os

from commands.paths import RAW_CORPUS_PATH, TOKENIZED_CORPUS_PATH, CLEANED_CORPUS_PATH
from corpus_builder.corpus_cleansing import CorpusCleansing


def cmd():
    src_path = os.path.join(RAW_CORPUS_PATH, 'en')
    dst_path = os.path.join(CLEANED_CORPUS_PATH, 'en')

    cleaner = CorpusCleansing()
    cleaner.clean_corpus(src_path, dst_path, True)


if __name__ == '__main__':
    cmd()
    print('Finished')
