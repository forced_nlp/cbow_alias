import os

BASE_PATH = os.path.dirname(os.path.realpath(__file__))

DATA_PATH = os.path.join(BASE_PATH, '..', 'data')

RAW_CORPUS_PATH = os.path.join(DATA_PATH, 'raw_corpus')

CLEANED_CORPUS_PATH = os.path.join(DATA_PATH, 'cleaned_corpus')

TOKENIZED_CORPUS_PATH = os.path.join(DATA_PATH, 'tokenized_corpus')
