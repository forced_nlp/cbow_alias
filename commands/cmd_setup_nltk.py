import nltk


def cmd():
    nltk.download('punkt')
    nltk.download('averaged_perceptron_tagger')
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    nltk.download('stopwords')


if __name__ == '__main__':
    cmd()
    print('Finished')
