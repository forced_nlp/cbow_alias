import os

from commands.paths import CLEANED_CORPUS_PATH, TOKENIZED_CORPUS_PATH
from corpus_builder.tokenized_corpus_builder import TokenizedCorpusBuilder


def cmd():
    src_path = os.path.join(CLEANED_CORPUS_PATH, 'en')
    pos_path = os.path.join(TOKENIZED_CORPUS_PATH, 'en', 'tokenized_corpus.txt')
    wrd_path = os.path.join(TOKENIZED_CORPUS_PATH, 'en', 'lemmatized_corpus.txt')
    builder = TokenizedCorpusBuilder()
    builder.build(src_path, pos_path, wrd_path, 'en')


if __name__ == '__main__':
    cmd()
    print('Finished')
