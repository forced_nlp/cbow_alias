import os
from typing import Any, Dict

from flask import current_app as app
from flask import render_template, request

from game.game_basics import GameBasics
from game.guessing_game import GuessingGame
from game.hinting_game import HintingGame
from model.dictionary_file_manager import DictionaryFileManager


mgr = DictionaryFileManager()
DICTIONARY = mgr.load('en')


@app.route('/')
def home():
    """Landing page."""
    return render_template(
        'home.html',
        title="CBOW Alias",
        description="CBOW Alias suggesting app"
    )


@app.route('/guessing_template')
def guessing_template():
    return render_template(
        'guessing_template.html',
    )


@app.route('/hinting_template')
def hinting_template():
    return render_template(
        'hinting_template.html',
    )


@app.route('/dictionary_template')
def dictionary_template():
    return render_template(
        'dictionary_template.html',
    )


@app.route('/guessing/')
def guessing():
    count = int(request.args.get('count', 7))
    variety = request.args.get('variety', GameBasics.VARIETY_POPULAR)

    game = GuessingGame(DICTIONARY)
    game.init_position(count, variety)

    return render_template(
        'guessing.html',
        position=game.pos.serialize()
    )


@app.route('/hinting/')
def hinting():
    count = int(request.args.get('count', 7))
    variety = request.args.get('variety', GameBasics.VARIETY_POPULAR)

    game = HintingGame(DICTIONARY)
    game.init_position(count, variety)
    hint = game.make_hint()
    print(hint)

    return render_template(
        'hinting.html',
        position=game.pos.serialize(),
        hint=hint.word,
        hint_count=hint.count
    )


@app.route('/solve_hint', methods=['POST'])
def solve_hint() -> Dict[str, Any]:
    data = request.json
    game = HintingGame(DICTIONARY)
    game.load_position(data['position'])
    if game.pos.game_over:
        return {"error": "Game is over"}

    status = game.process_guess(data['words'])
    hint = game.make_hint()
    print(hint)

    return {
        "position": game.pos.serialize(),
        "status": status,
        "hint": hint.word,
        "hint_count": hint.count
    }


@app.route('/get_hint_options', methods=['POST'])
def get_hint_options() -> Dict[str, Any]:
    data = request.json
    game = GuessingGame(DICTIONARY)
    game.load_position(data['position'])
    return {"hints": game.filter_hints(data['word'], 10)}


@app.route('/give_hint', methods=['POST'])
def give_hint() -> Dict[str, Any]:
    log_path = os.path.dirname(os.path.realpath(__file__))
    log_path = os.path.join(log_path, '..',
                            'model', 'tests', 'optimization',
                            'guessing_optimize.csv')

    data = request.json
    game = GuessingGame(DICTIONARY, position_log_path=log_path)
    game.load_position(data['position'])
    status = game.process_hint(data['hint'], data['count'])
    return {
        "position": game.pos.serialize(),
        "status": status
    }


@app.get('/word_lookup/')
def word_lookup():
    prefix = request.args.get('prefix')
    count = request.args.get('count', 10)

    words = []
    for w in DICTIONARY.words:
        if w.word.startswith(prefix):
            words.append(w)
            if len(words) >= count:
                break

    return {"words": words}


@app.get('/word_lookup/')
def word_math():
    formula = request.args.get('formula')
    return {}
