class GameTemplatePageManager {
    constructor() {
        document.getElementById('play_button').addEventListener('click', e => {
                this.play();
            });
        this.link = null;
    }

    play() {
        let count = document.getElementById('input_words').value;
        let variety = document.getElementById('input_variety').value;
        let link = this.link.replace(/\/$/g, '');
        link = `${link}/?count=${count}&variety=${variety}`;
        window.location = link;
    }
}