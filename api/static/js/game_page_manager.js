class GamePageManager {
    constructor(positionStr) {
        this.position = JSON.parse(positionStr);
    }

    processTurnResponse(response) {
        if (response.status.error) {
            console.log(response.status.error);
            alert(response.status.error);
            return;
        }
        this.position = JSON.parse(response.position);
        this.renderPosition();
    }

    renderTable() {
        let markup = '<table id="position_table" class="game_table">';
        let cells = this.buildCells();

        cells.forEach((row, r) => {
            markup += ' <tr>';
            row.forEach((cell, c) => {
                markup += this.renderCell(cell, r, c);
                });
            markup += ' </tr>';
        });
        markup += ' </table>';
        this.updateElementMarkup('position_placeholder', markup);
    }

    renderCell(cell, r, c) {
        throw '"renderCell()" is not implemented'
    }

    buildCells() {
        throw '"buildCells()" is not implemented'
    }

    renderTurns() {
        let markup = '';
        if (this.position.turns) {
            markup = '<table class="turns">';
            for (let i = this.position.turns.length-1; i >= 0; i--) {
                let turn = this.position.turns[i];
                markup += `<tr> <td>Turn ${i + 1}</td>`;
                markup += `<td> ${this.escapeWord(turn.hint)} x${turn.count} </td>`;
                markup += `<td> ${this.escapeWord(turn.message)} </td> </tr>`;
            }
            markup += '</table>';
        }
        this.updateElementMarkup('turns_placeholder', markup);
    }

    updateElementMarkup(elementId, markup) {
        let targetDiv = document.getElementById(elementId);
        targetDiv.innerHTML = markup;
    }

    escapeWord(wrd) {
        return wrd.replaceAll('\\"', '"');
    }

    strHash(s) {
        let hash = 0;
        if (s.length === 0) return hash;
        for (let i = 0; i < s.length; i++) {
            let chr = s.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }
}