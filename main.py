import os
import codecs
from commands.paths import DATA_PATH
from model.dictionary_file_manager import DictionaryFileManager


def list_nouns():
    mgr = DictionaryFileManager()
    dc = mgr.load('en')
    nouns = [w for w in dc.words if w.pos == "NN"]
    nouns.sort(key=lambda w: w.frequency, reverse=True)

    path = os.path.join(DATA_PATH, "other", "nouns.txt")
    with codecs.open(path, mode='w', encoding='utf-8') as fw:
        for noun in nouns:
            fw.write(f"{noun.word}\n")


if __name__ == '__main__':
    list_nouns()
