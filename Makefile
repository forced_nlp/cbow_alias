pack-all:
	cd data/tokenized_corpus/en/ && zip corpus.zip ./*.txt && split -b 45M corpus.zip corpus_parts.
	cd data/dictionary/ && zip dict_en.zip en.msgpack && split -b 45M dict_en.zip dict_en_parts.
	cd data/raw_corpus/en/ && zip gutenberg.zip ./*.txt && split -b 45M gutenberg.zip gutenberg_en_parts.

unpack-dictionary:
	cd data/dictionary/ && cat dict_en_parts.?? > dict_en.zip && unzip -n dict_en.zip

unpack-all: unpack-dictionary
	cd data/tokenized_corpus/en/ && cat corpus_parts.?? > corpus.zip && unzip -n corpus.zip
	cd data/raw_corpus/en/ && cat gutenberg_en_parts.?? > gutenberg.zip && unzip -n gutenberg.zip

shrink:
	cd data/tokenized_corpus/en/ && rm -rf ./*.txt && rm -f corpus.zip
	cd data/dictionary/ && rm -f ./*.msgpack && rm -f dict_en.zip
	cd data/raw_corpus/en/ && rm -f ./*.txt && rm -f gutenberg.zip

setup-nltk:
	python -m commands.cmd_setup_nltk

corpus-build-en:
	python -m commands.cmd_build_corpus_en

dictionary-build-en:
	python -m commands.cmd_build_dictionary_en