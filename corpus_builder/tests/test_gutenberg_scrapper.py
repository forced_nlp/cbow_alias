from unittest import TestCase
import os

from corpus_builder.gutenberg_scrapper import GutenbergScrapper
from commands.paths import DATA_PATH


class TestGutenbergScrapper(TestCase):
    def test_scrap_top(self):
        out_path = os.path.join(DATA_PATH, "raw_corpus/en")

        scr = GutenbergScrapper(out_path)
        #scr.scrap_list_page("https://www.gutenberg.org/browse/scores/top#books-last1")
        scr.scrap_list_page("https://www.gutenberg.org/browse/scores/top#books-last30")

    def test_scrap_sequential(self):
        out_path = os.path.join(DATA_PATH, "raw_corpus")
        scr = GutenbergScrapper(out_path)

        ok, failed, skipped = 0, 0, 0
        start, stop = 601, 900
        for i in range(start, stop):
            try:
                downloaded = scr.pull_book(i)
                if not downloaded:
                    skipped += 1
                else:
                    ok += 1
            except:
                failed += 1
            print(f'{ok + failed + skipped} of {stop - start + 1}, '
                  f'failed: {failed}, skipped: {skipped}, ok: {ok}')
