import codecs
from typing import List, Set, Tuple

from gensim.models import Word2Vec


VECTOR_SIZE = 50


class CorpusVectorizer:
    def __init__(self):
        self.sentences: List[List[str]] = []
        self.encoding = 'utf-8'
        self.words: Set[str] = set()
        self.word_to_vec: List[Tuple[str, List[float]]] = []

    def build(self,
              corpus_file_path: str,
              output_file_path: str):
        self._read_tokenized_corpus(corpus_file_path)
        self._calculate_vectors()
        self._store(output_file_path)

    def _read_tokenized_corpus(
            self,
            corpus_file_path: str):
        sentence_num = ''
        sentence: List[str] = []
        with codecs.open(corpus_file_path, mode='r', encoding=self.encoding) as fr:
            lines = fr.readlines()
        for line in lines:
            parts = line.split(',')
            word, _, sent_num = parts[0], parts[1], parts[2]
            self.words.add(word)
            if sent_num != sentence_num:
                if sentence:
                    self.sentences.append(sentence)
                    sentence = []
                sentence_num = sent_num
            else:
                sentence.append(word)

    def _calculate_vectors(self):
        model = Word2Vec(sentences=self.sentences, vector_size=VECTOR_SIZE, window=5, min_count=1, workers=4)
        for word in self.words:
            try:
                v = model.wv[word]
            except:
                continue
            self.word_to_vec.append((word, v))

    def _store(self, path: str):
        with codecs.open(path, mode='w', encoding=self.encoding) as fw:
            for word, v in self.word_to_vec:
                vector_str = ','.join([f'{val:.4f}' for val in v])
                fw.write(f'{word};{vector_str}\n')
