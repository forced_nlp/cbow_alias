import codecs
import os
from typing import List, Dict, Tuple, Optional, Set

import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords

from model.alphabet import alphabet_by_code

"""
MD Modal
NN Noun, singular or mass
NNS Noun, plural
NNP Proper noun, singular
NNPS Proper noun, plural

RB Adverb
RBR Adverb, comparative
RBS Adverb, superlative

VB Verb, base form
VBD Verb, past tense
VBG Verb, gerund or present participle
VBN Verb, past participle
VBP Verb, non­3rd person singular present
VBZ Verb, 3rd person singular present
"""

ENABLED_TOKENS = {
    'MD', 'NN', 'NNS',
     # 'NNP',  # personal nomen
    'NNPS', 'VB', 'VBD', 'VBG',
    'VBN', 'VBP', 'VBZ'
}


WORDNET_POS_BY_POS = {
    t: nltk.corpus.reader.VERB if t.startswith('V') else nltk.corpus.reader.NOUN
    for t in ENABLED_TOKENS
}

PERCENT_TOO_COMMON_TO_REMOVE = 0.001
PERCENT_TOO_RARE_TO_REMOVE = 30


class TokenizedCorpusBuilder:
    def __init__(self):
        self.encoding = 'utf-8'
        self.lang_code = ''
        self.lemmatizer = WordNetLemmatizer()
        self._word_lemmas: Dict[str, str] = {}
        self.tokenized_output_file_name = ''
        self.lemmatized_output_file_name = ''
        self.sentence_counter = 0
        self.alphabet = alphabet_by_code['en']
        self.stop_words: Set[str] = set(stopwords.words('english'))

    def build(self,
              cleaned_corpus_folder: str,
              tokenized_output_file_name: str,
              lemmatized_output_file_name: str,
              lang_code: str):
        self.lang_code = lang_code
        self.alphabet = alphabet_by_code[lang_code]
        self.tokenized_output_file_name = tokenized_output_file_name
        self.lemmatized_output_file_name = lemmatized_output_file_name
        self.sentence_counter = 0

        paths: List[str] = []
        print(f'Reading corpus [{cleaned_corpus_folder}]')
        self._read_file_paths(cleaned_corpus_folder, paths)
        with codecs.open(tokenized_output_file_name, mode='w', encoding=self.encoding) as fw_tok:
            with codecs.open(lemmatized_output_file_name, mode='w', encoding=self.encoding) as fw_lem:
                for i, file_name in enumerate(paths):
                    print(f'Processing {i+1} of {len(paths)}: [{file_name}]')
                    self._process_file(file_name, fw_tok, fw_lem)
        self._filter_too_common_words()

    def _filter_too_common_words(self):
        word_count: Dict[str, int] = {}
        with codecs.open(self.tokenized_output_file_name, mode='r', encoding=self.encoding) as fr:
            lines = fr.readlines()
        file_items: List[Tuple[str, str, str]] = []
        for line in lines:
            parts = line.split(',')
            word, pos, sent_num = parts[0], parts[1], parts[2]
            file_items.append((word, pos, sent_num))
            count = word_count.get(word, 0)
            word_count[word] = count + 1

        word_count_list = list(word_count.items())
        word_count_list.sort(key=lambda wc: wc[1], reverse=True)
        remove_common_count = round(len(word_count_list) * PERCENT_TOO_COMMON_TO_REMOVE / 100)
        remove_rare_count = round(len(word_count_list) * PERCENT_TOO_RARE_TO_REMOVE / 100)
        words_too_common = {w[0] for w in word_count_list[:remove_common_count]}
        words_too_rare = {w[0] for w in word_count_list[-remove_rare_count:]}
        words_to_remove = words_too_common.union(words_too_rare)

        with codecs.open(self.tokenized_output_file_name, mode='w', encoding=self.encoding) as fw:
            for word, pos, sent_num in file_items:
                if word in words_to_remove:
                    continue
                fw.write(f'{word},{pos},{sent_num}')

    def _process_file(self,
                      file_path: str,
                      fw_tok: codecs.StreamReaderWriter,
                      fw_lem: codecs.StreamReaderWriter):
        with codecs.open(file_path, mode='r', encoding=self.encoding) as fr:
            sentences = fr.readlines()
        for sentence in sentences:
            self._process_text_block(sentence, fw_tok, fw_lem)

    def _process_text_block(self,
                            block: str,
                            fw_tok: codecs.StreamReaderWriter,
                            fw_lem: codecs.StreamReaderWriter):
        if not block:
            return
        tokens = nltk.word_tokenize(block)
        if not tokens:
            return
        pos_tokens = nltk.pos_tag(tokens)
        for wrd, pos in pos_tokens:
            wrd_lower = wrd.lower().strip('.,\'"_-$&%^*')
            if not wrd_lower:
                continue
            if wrd in self.stop_words:
                continue
            lemma = self._get_word_lemma(wrd_lower, pos)
            if lemma:
                fw_tok.write(f'{lemma},{pos},{self.sentence_counter}\n')
            fw_lem.write(f'{lemma or wrd_lower},{self.sentence_counter}\n')
        self.sentence_counter += 1

    def _get_word_lemma(self, wrd: str, pos: str) -> Optional[str]:
        if pos not in ENABLED_TOKENS:
            return None
        if self._filter_word_out(wrd):
            return None
        wordnet_pos = WORDNET_POS_BY_POS[pos]
        return self.lemmatizer.lemmatize(wrd, pos=wordnet_pos)

    @classmethod
    def _read_file_paths(cls, corpus_folder: str, paths: List[str]):
        paths += [os.path.join(corpus_folder, o) for o in os.listdir(corpus_folder)
                  if not os.path.isdir(os.path.join(corpus_folder, o))
                  and os.path.splitext(o)[1].lower() == '.txt']
        for sf in cls._read_subfolders(corpus_folder):
            cls._read_file_paths(sf, paths)

    @classmethod
    def _read_subfolders(cls, corpus_folder):
        subfolders = [os.path.join(corpus_folder, o) for o in os.listdir(corpus_folder)
                      if os.path.isdir(os.path.join(corpus_folder, o))]
        return subfolders

    def _filter_word_out(self, wrd: str) -> bool:
        if len(wrd) < 3:
            return True
        if not self.alphabet.reg_whole_word.match(wrd):
            return True
        return False
