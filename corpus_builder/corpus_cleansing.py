import codecs
import os
from typing import List
from neuspell import BertChecker

from corpus_builder.lexnlp.sentences import get_sentence_list


class CorpusCleansing:
    def __init__(self):
        self.encoding = 'utf-8'
        self.lang_code = 'en'
        self.process_existing = False
        self.checker = BertChecker()
        self.checker.from_pretrained(
            weights="/home/andrey/sources/andrey/cbow_alias/commands/subwordbert-probwordnoise")

    def clean_corpus(
        self, src_folder: str, dst_folder: str,
        correct_spelling: bool) -> None:
        """
        Split text into sentences.
        Optionally correct spelling in sentences.
        Write sentences to file line by line.
        """
        paths: List[str] = []
        print(f'Reading corpus [{src_folder}]')
        self._read_file_paths(src_folder, paths)
        for i, file_path in enumerate(paths):
            print(f'Processing {i + 1} of {len(paths)}: [{file_path}]')
            self._process_file(file_path, dst_folder, correct_spelling)
        print(f'Processing complete')

    def _process_file(self, file_path: str, dst_folder: str, correct_spelling: bool) -> None:
        file_name = os.path.basename(file_path)
        out_path = os.path.join(dst_folder, file_name)
        if not self.process_existing and os.path.isfile(out_path):
            return
        # read file, split by sentences
        with codecs.open(file_path, mode='r', encoding=self.encoding) as fr:
            file_text = fr.read()
        sentences = get_sentence_list(file_text)

        with codecs.open(out_path, mode='w', encoding=self.encoding) as fw:
            for s in sentences:
                s = self._clean_sentence(s)
                s = self.checker.correct(s)
                fw.write(s + '\n')

    @classmethod
    def _clean_sentence(cls, s: str) -> str:
        return s.replace('\r', ' ').strip(' \n').replace('\n', ' ').replace('  ', ' ')

    @classmethod
    def _read_file_paths(cls, corpus_folder: str, paths: List[str]):
        paths += [os.path.join(corpus_folder, o) for o in os.listdir(corpus_folder)
                  if not os.path.isdir(os.path.join(corpus_folder, o))
                  and os.path.splitext(o)[1].lower() == '.txt']
        for sf in cls._read_subfolders(corpus_folder):
            cls._read_file_paths(sf, paths)

    @classmethod
    def _read_subfolders(cls, corpus_folder):
        subfolders = [os.path.join(corpus_folder, o) for o in os.listdir(corpus_folder)
                      if os.path.isdir(os.path.join(corpus_folder, o))]
        return subfolders