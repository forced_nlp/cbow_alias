import os
from typing import List
from urllib.request import urlopen
import re
import codecs


class GutenbergScrapper:
    LINK_PATTERN = 'https://www.gutenberg.org/ebooks/'
    HOME_PATTERN = 'https://www.gutenberg.org/'
    LINK_RE = re.compile(r'(?<=<a href="/ebooks/)\d+')
    FILE_LINK_RE = re.compile(r'(?<=<a href="/)files/\d+/[\d-]+\.txt(?=")')

    ENCODING = 'utf-8'

    def __init__(self,
                 output_folder: str,
                 skip_existing: bool = True):
        self.output_folder = output_folder
        self.chars_to_skip = 4700
        self.skip_existing = skip_existing

    def scrap_list_page(self, url: str):
        markup = self._get_body(url)

        book_ids: List[int] = []
        for entry in self.LINK_RE.finditer(markup):
            book_ids.append(int(entry.group()))
        print(f'{len(book_ids)} books to scrap')

        done, failed = 0, 0
        for book_id in book_ids:
            downloaded = 0
            try:
                downloaded += self.pull_book(book_id)
                done += 1
            except:
                failed += 1
            suffix = '; skipped' if not downloaded else ''
            print(f'{done + failed} of {len(book_ids)} are ready, {failed} failed{suffix}')
        print('Done scrapping')

    def pull_book(self, id: int) -> int:
        url = f'{self.LINK_PATTERN}{id}'
        markup = self._get_body(url)
        total = 0
        for entry in self.FILE_LINK_RE.finditer(markup):
            total += self.download_plain_text_file(entry.group(0))
        return total

    def download_plain_text_file(self, file_path: str) -> bool:
        # file path example: files/74/74-0.txt
        file_name = os.path.basename(file_path)
        out_path = os.path.join(self.output_folder, file_name)
        if self.skip_existing and os.path.isfile(out_path):
            return False

        url = f'{self.HOME_PATTERN}{file_path}'
        text = self._get_body(url)
        text = text[self.chars_to_skip:]
        if len(text) < 10000:
            return False
        with codecs.open(out_path, 'w', encoding=self.ENCODING) as fw:
            fw.write(text)
        return True

    @classmethod
    def _get_body(cls, url: str) -> str:
        f = urlopen(url)
        return f.read().decode(cls.ENCODING)