import codecs
import datetime
import os
from typing import Dict, List

from game.game_basics import GameBasics
from model.dictionary import Dictionary
from model.position import TurnLogRecord
from model.position_solver import PositionSolver
from model.word_card import WordCard


class GuessingGame(GameBasics):
    def __init__(self,
                 dictionary: Dictionary,
                 side: str = 'a',
                 position_log_path: str = ''):
        super().__init__(dictionary, side)
        self.position_log_path = position_log_path

    def process_hint(self, hint: str, count: int) -> Dict[str, str]:
        if self.pos.game_status:
            return {
                'error': f"Game's over: {self.pos.game_status}"
            }

        card = self.word_card.get(hint)
        if not card:
            return {
                'error': f'Hint "{hint}" is not found in the dictionary'
            }

        check_hint_status, check_error = self.check_hint(hint)
        if not check_hint_status:
            return {
                'error': check_error
            }

        words_to_guess = self.pos.words_to_solve
        solver = PositionSolver(
            self.dictionary.words,
            self.word_card
        )
        start = datetime.datetime.now()
        solution = solver.solve(
            words_to_guess,
            hint,
            count,
            side=self.side)[:count]
        elapsed = (datetime.datetime.now() - start).total_seconds()
        print(f"Elapsed: {elapsed}s")  # 4.86 initially

        if not solution:
            message = f'Hint "{hint}" gave no clue'
        else:
            message = self.update_position(solution)
        self.pos.turns_made.append(TurnLogRecord(
            hint=hint,
            count=count,
            message=message
        ))
        self._log_position_hint(hint, count, words_to_guess, solution)
        self.update_game_status()
        if self.pos.game_status:
            message = f'{message}. {self.pos.game_status}'
        return {
            'message': message
        }

    def _log_position_hint(self,
                           hint: str,
                           count: int,
                           words: List[WordCard],
                           solution: List[str]) -> None:
        if not self.position_log_path:
            return
        file_empty = not os.path.isfile(self.position_log_path)

        def stringify_list(lst: List[str]) -> str:
            return ';'.join(lst)

        def stringify_card_list(lst: List[WordCard]) -> str:
            return ';'.join([w.word for w in lst])

        with codecs.open(self.position_log_path, 'a+', encoding='utf-8') as fw:
            if file_empty:
                fw.write("words,hint,count,solution\n")
            fw.write(f"{stringify_card_list(words)},{hint},{count},"
                     f"{stringify_list(solution[:count+2])}\n")
