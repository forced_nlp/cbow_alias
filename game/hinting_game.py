from typing import List

from game.game_basics import GameBasics
from model.dictionary import Dictionary
from model.hint_maker import HintMaker, WordHint
from model.hint_maker_bf import HintMakerBf
from model.position import TurnLogRecord


class HintingGame(GameBasics):
    def __init__(self,
                 dictionary: Dictionary,
                 side: str = 'a'):
        super().__init__(dictionary, side)

    def make_hint(self) -> WordHint:
        maker = HintMakerBf(
            self.dictionary.words,
            self.word_card
        )
        return maker.give_hint(self.pos, self.side)

    def process_guess(self, words: List[str]) -> str:
        msg = self.update_position(words)
        self.update_game_status()
        self.pos.turns_made.append(TurnLogRecord(
            hint=', '.join(words),
            count=0,
            message=msg
        ))
        return msg
