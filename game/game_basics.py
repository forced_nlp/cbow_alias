import random
from typing import Optional, List, Dict, Any, Tuple

from model.dictionary import Dictionary
from model.position import Position, WordGuessed


class GameBasics:
    VARIETY_POPULAR = 'popular'
    VARIETY_MIXED = 'mixed'
    VARIETY_RARE = 'rare'

    def __init__(self,
                 dictionary: Dictionary,
                 side: str = 'a'):
        self.side = side
        self.dictionary = dictionary
        self.word_card = {w.word: w for w in self.dictionary.words}
        self.pos: Optional[Position] = None
        self.words = [w.word for w in self.dictionary.words]
        self.words.sort()

    def init_position(self,
                      count: int = 7,
                      variety: str = 'popular') -> None:
        min_rank, max_rank = 0.05, 1.2
        if variety == self.VARIETY_MIXED:
            min_rank, max_rank = 0.15, 2
        if variety == self.VARIETY_RARE:
            min_rank, max_rank = 0.25, 2.5

        self.pos = Position()
        total = len(self.dictionary.words)
        min_rank = min_rank / 100 * total
        max_rank = max_rank / 100 * total

        all_words = [w for w in self.dictionary.words if min_rank <= w.frequency_rank <= max_rank]
        all_words = random.sample(all_words, count * 3 + 1)
        self.pos.words_a = all_words[:count]
        self.pos.words_b = all_words[count: count*2]
        self.pos.words_n = all_words[count*2:-1]
        self.pos.words_mine = [all_words[-1]]

    def load_position(self, position: Dict[str, Any]) -> None:
        self.pos = Position.deserialize(position, self.word_card)

    def filter_hints(self, wrd: str, max_len=10) -> List[str]:
        # TODO: filter by matching / stemming
        wrd = wrd.lower()
        results = []
        for w in self.words:
            if not w.startswith(wrd):
                if results:
                    return results
                continue
            results.append(w)
            if len(results) == max_len:
                return results
        return results

    def check_hint(self, hint: str) -> Tuple[bool, str]:
        # check for +/- the same words
        for w in self.pos.all_words:
            if w == hint:
                return False, f'The hint ({hint}) is already among the words'
            if w in hint:
                return False, f'Word "{w}" is a part from the hint ({hint})'
            if hint in w:
                return False, f'Hint "{hint}" is a part from the hint ({w})'
        # TODO: check +/- one letter
        return True, ''

    def update_position(self,
                        guessed_words: List[str],
                        side: str = 'a') -> str:
        messages: List[str] = []
        guessed_words_set = set(guessed_words)

        mine_triggered = ''
        for w in self.pos.words_mine:
            if w.word in guessed_words_set:
                mine_triggered = w.word
                break
        if mine_triggered:
            self.pos.words_guessed.append(WordGuessed(
                mine_triggered, 'm', False
            ))
            self.pos.words_mine = [w for w in self.pos.words_mine if w.word != mine_triggered]
            return f'Word mine "{mine_triggered}" triggered. Game over'

        word_side = {
            'a': self.pos.words_a,
            'b': self.pos.words_b,
            'n': self.pos.words_n,
        }
        for sd, words in word_side.items():
            for w in words:
                if w.word not in guessed_words_set:
                    continue
                if side == sd:
                    messages.append(f'Word "{w.word}" was a right guess')
                else:
                    messages.append(f'Word "{w.word}" was a wrong guess')
                self.pos.words_guessed.append(WordGuessed(
                    w.word, sd, side == sd
                ))
        self.pos.words_a = [w for w in self.pos.words_a if w.word not in guessed_words_set]
        self.pos.words_b = [w for w in self.pos.words_b if w.word not in guessed_words_set]
        self.pos.words_n = [w for w in self.pos.words_n if w.word not in guessed_words_set]

        return '. '.join(messages)

    def update_game_status(self):
        mines = [w.word for w in self.pos.words_guessed if w.word_side == 'm']
        if mines:
            # mine word guessed
            self.pos.game_status = f'Failed: mine {mines[0]} triggered'
            return
        target_words = self.pos.words_a if self.side == 'a' else self.pos.words_b
        if not target_words:
            self.pos.game_status = 'Game finished'
