import json
import re
from typing import List, Dict, Any

from model.word_card import WordCard


REG_QUOTES = re.compile(r'(?<!\\)"')


def escape_word(wrd: str) -> str:
    return REG_QUOTES.sub(r'\"', wrd)


class WordGuessed:
    def __init__(self,
                 word: str,
                 word_side: str,
                 guessed_correct: bool):
        self.word: str = word
        self.word_side: str = word_side
        self.guessed_correct = guessed_correct

    def __repr__(self) -> str:
        guess_status = ': wrong' if not self.guessed_correct else ''
        return f'{self.word} ({self.word_side}){guess_status}'

    def __str__(self) -> str:
        return self.__repr__()

    def to_dict(self) -> Dict[str, Any]:
        return {
            "word": escape_word(self.word),
            "side": self.word_side,
            "correct": self.guessed_correct
        }


class TurnLogRecord:
    def __init__(self,
                 hint: str,
                 count: int,
                 message: str):
        self.hint = hint
        self.count = count
        self.message = message

    def __repr__(self) -> str:
        return f'{self.hint} ({self.count}). {self.message}'

    def __str__(self) -> str:
        return self.__repr__()

    def to_dict(self) -> Dict[str, Any]:
        return {
            "hint": escape_word(self.hint),
            "count": self.count,
            "message": escape_word(self.message)
        }


class AiSettings:
    GUESS_MODE_RISKY = 'risky'
    GUESS_MODE_CONFIDENT = 'confident'
    GUESS_MODE_TAKE_ALL = 'take_all'

    def __init__(self,
                 guess_mode: str):
        # GUESS_MODE_...
        self.guess_mode = guess_mode

    def __repr__(self) -> str:
        return f'{self.guess_mode}'

    def __str__(self) -> str:
        return self.__repr__()

    @classmethod
    def get_default_ai_settings(cls) -> 'AiSettings':
        return AiSettings(cls.GUESS_MODE_RISKY)

    def to_dict(self) -> Dict[str, Any]:
        return {
            "guess_mode": self.guess_mode
        }


class Position:
    def __init__(self):
        self.words_a: List[WordCard] = []
        self.words_b: List[WordCard] = []
        self.words_n: List[WordCard] = []
        self.words_mine: List[WordCard] = []
        self.words_guessed: List[WordGuessed] = []
        self.turns_made: List[TurnLogRecord] = []
        self.ai_settings: AiSettings = AiSettings.get_default_ai_settings()
        self.game_status: str = ''

    def __repr__(self) -> str:
        pos = 'a: [' + ', '.join([w.word for w in self.words_a]) + ']\n' + \
              'b: [' + ', '.join([w.word for w in self.words_b]) + ']\n' + \
              'n: [' + ', '.join([w.word for w in self.words_n]) + ']\n' + \
              'mine: [' + ', '.join([w.word for w in self.words_mine]) + '], ' + \
              f'{len(self.turns_made)} turns'
        if self.game_status:
            pos = f'{self.game_status}. {pos}'
        return pos

    def __str__(self) -> str:
        return self.__repr__()

    @property
    def game_over(self) -> bool:
        return bool(self.game_status)

    @property
    def all_words(self) -> List[str]:
        return [w.word for w in self.words_a] + [w.word for w in self.words_b] + \
               [w.word for w in self.words_n] + [w.word for w in self.words_mine] + \
               [w.word for w in self.words_guessed]

    @property
    def words_to_solve(self) -> List[WordCard]:
        return list(self.words_a) + list(self.words_b) + \
               list(self.words_n) + list(self.words_mine)

    def serialize(self) -> str:
        data = {
            'words_a': [escape_word(w.word) for w in self.words_a],
            'words_b': [escape_word(w.word) for w in self.words_b],
            'words_n': [escape_word(w.word) for w in self.words_n],
            'words_mine': [escape_word(w.word) for w in self.words_mine],
            'words_guessed': [w.to_dict() for w in self.words_guessed],
            'turns': [t.to_dict() for t in self.turns_made],
            'ai': self.ai_settings.to_dict(),
            'status': self.game_status
        }
        return json.dumps(data)

    @classmethod
    def deserialize(cls,
                    position: Dict[str, Any],
                    word_card: Dict[str, WordCard]):
        pos = Position()
        pos.words_a = [word_card[w] for w in position['words_a']]
        pos.words_b = [word_card[w] for w in position['words_b']]
        pos.words_n = [word_card[w] for w in position['words_n']]
        pos.words_mine = [word_card[w] for w in position['words_mine']]
        pos.words_guessed = [
            WordGuessed(
                w['word'], w['side'], w['correct']
            ) for w in position['words_guessed']
        ]
        pos.turns_made = [
            TurnLogRecord(
                t['hint'], t['count'], t['message']
            ) for t in position['turns']
        ]
        pos.ai_settings = AiSettings(
            position['ai']['guess_mode']
        )
        pos.game_status = position['status']
        return pos
