from typing import Dict, List, Tuple
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

from model.word_card import WordCard


class WordSimilarityTagged:
    def __init__(self, word_a: str, word_b: str,
                 tag: str, coeff: float):
        self.word_a = word_a
        self.word_b = word_b
        self.tag = tag
        self.coeff = coeff
        self.index: Tuple[int, int] = (0, 0)
        self.similarity: float = 0

    def __repr__(self) -> str:
        return f'"{self.word_a}": "{self.word_b}" ({self.tag}) -> {self.similarity:.4f}'

    def __str__(self) -> str:
        return self.__repr__()


class WordSimilarityCalculator:
    def __init__(self, word_to_card: Dict[str, WordCard]):
        self.word_to_card = word_to_card
        self.similarity_by_word: Dict[str, List[WordSimilarityTagged]] = {}

    def append(self, word: str,
               word_a: str, word_b: str,
               tag: str, coeff: float) -> None:
        word_sim = self.similarity_by_word.get(word)
        if not word_sim:
            word_sim = []
            self.similarity_by_word[word] = word_sim
        word_sim.append(WordSimilarityTagged(word_a, word_b, tag, coeff))

    def get_most_similar(self) -> List[Tuple[str, WordSimilarityTagged]]:
        """
        :return: the first word is most similar, the second is slightly less similar etc
        """
        a_values, b_values = [], []
        a_index, b_index = {}, {}

        for _, sim_list in self.similarity_by_word.items():
            sim_rec: WordSimilarityTagged
            for sim_rec in sim_list:
                ix = a_index.get(sim_rec.word_a, -1)
                if ix < 0:
                    a_values.append(self.word_to_card[sim_rec.word_a].vector)
                    ix = len(a_values) - 1
                    a_index[sim_rec.word_a] = ix
                iy = b_index.get(sim_rec.word_b, -1)
                if iy < 0:
                    b_values.append(self.word_to_card[sim_rec.word_b].vector)
                    iy = len(b_values) - 1
                    b_index[sim_rec.word_b] = iy
                sim_rec.index = ix, iy

        m = cosine_similarity(np.array(a_values), np.array(b_values))

        word_sim: List[Tuple[str, WordSimilarityTagged]] = []
        for word, sim_list in self.similarity_by_word.items():
            sim_rec: WordSimilarityTagged
            for sim_rec in sim_list:
                sim_rec.similarity = m[sim_rec.index[0]][sim_rec.index[1]]
                sim_rec.similarity *= sim_rec.coeff
            sim_list.sort(key=lambda r: r.similarity, reverse=True)
            word_sim.append((word, sim_list[0]))

        # sort and return the best shot
        word_sim.sort(key=lambda w: w[1].similarity, reverse=True)
        return word_sim
