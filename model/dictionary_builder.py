import codecs
import collections
import math
from typing import List, Tuple, Dict, Optional

from gensim.models import Word2Vec
import numpy as np
from sklearn.metrics.pairwise import cosine_distances

from model.dictionary import Dictionary
from model.word_card import WordCard

# (100, 3)
VECTOR_SIZE = 128
WINDOW_SIZE = 4
SENTENCE_LENGTH = 30

NEIGHBOURS_COUNT = 30
SYNONYM_COUNT = 10


class DictionaryBuilder:
    def __init__(self):
        self.encoding = 'utf-8'
        self.sentences: List[List[Tuple[str, str]]] = []
        self.card_by_word: Dict[str, WordCard] = {}
        self.cards: List[WordCard] = []
        self.w2v_model: Optional[Word2Vec] = None

    def build(self,
              lang: str,
              tokenized_corpus_path: str,
              lemmatized_corpus_path: str):
        self._read_sentences(tokenized_corpus_path, self.sentences)
        self._populate_cards()
        print("start calculating vectors")
        self._calculate_vectors(lemmatized_corpus_path)

        self.cards = list(self.card_by_word.values())
        print("start searching for neighbours")
        self._find_words_neighbours()
        print("start searching for synonyms")
        self._find_synonyms()
        dictionary = Dictionary(lang)
        dictionary.words = list(self.card_by_word.values())
        print("dictionary's built")
        return dictionary
        
    def _populate_cards(self):
        for sentence in self.sentences:
            for word, pos in sentence:
                card = self.card_by_word.get(word)
                if card:
                    card.frequency += 1
                else:
                    card = WordCard(
                        word=word,
                        pos=pos,
                        frequency=1)
                    self.card_by_word[word] = card

        # calculate frequency and relative frequency
        card_count = len(self.card_by_word)
        freq_set = set()
        for card in self.card_by_word.values():
            freq_set.add(card.frequency)
        freqs = list(freq_set)
        freqs.sort(reverse=True)
        freq_rank = {freqs[i]: i for i in range(len(freqs))}
        for card in self.card_by_word.values():
            card.frequency_rank = freq_rank[card.frequency]

        for card in self.card_by_word.values():
            card.frequency /= card_count
            card.frequency_rel_rank = 100 * card.frequency_rel_rank / card_count

    def _calculate_vectors(self, lemmatized_corpus_path: str):
        sentence_tuples: List[Tuple[str]] = []
        self._read_sentences(lemmatized_corpus_path, sentence_tuples)

        def make_eqsize_sents(sentence_tuples: List[Tuple[str]], sent_len) -> List[List[str]]:
            sentences: List[List[str]] = []
            sents: List[str] = []
            for s in sentence_tuples:
                for w in s:
                    sents.append(w[0])
                    if len(sents) == sent_len:
                        sentences.append(sents)
                        sents = []
            if sents:
                sentences.append(sents)
            return sentences

        sentences = [[w[0] for w in s] for s in sentence_tuples]
        # sentences = make_eqsize_sents(sentence_tuples, SENTENCE_LENGTH)
        model = Word2Vec(sentences=sentences,
                         vector_size=VECTOR_SIZE,
                         window=WINDOW_SIZE,
                         min_count=1,
                         workers=4)
        self.w2v_model = model
        for word in list(self.card_by_word.keys()):
            try:
                v = model.wv[word]
                self.card_by_word[word].vector = list(v.astype(float))
            except:
                # remove the word we could find the card for
                del self.card_by_word[word]

    def _read_sentences(
            self,
            corpus_file_path: str,
            sentences: List[List[Tuple[str, ...]]]):
        sentence_num = ''
        sentence: List[Tuple[str, ...]] = []
        with codecs.open(corpus_file_path, mode='r', encoding=self.encoding) as fr:
            lines = fr.readlines()
        for line in lines:
            parts = line.split(',')
            if len(parts) == 3:
                word, pos, sent_num = parts[0], parts[1], parts[2]
                if sent_num != sentence_num:
                    if sentence:
                        sentences.append(sentence)
                        sentence = []
                    sentence_num = sent_num
                sentence.append((word, pos))
            else:
                word, sent_num = parts[0], parts[1]
                if sent_num != sentence_num:
                    if sentence:
                        sentences.append(sentence)
                        sentence = []
                    sentence_num = sent_num
                sentence.append((word,))
        if sentence:
            sentences.append(sentence)

    def _find_words_neighbours(self):
        # flatten sentences
        words = []
        for sent in self.sentences:
            for word, pos in sent:
                words.append(word)

        word_neighbours: Dict[str, Dict[str, float]] = {}
        window = collections.deque(maxlen=3)
        # this is an alternative for TF-IDF
        word_inv_frequency = {w.word: 1 / w.frequency**0.15 for w in self.cards}
        word_index = {self.cards[i].word: i for i in range(len(self.cards))}

        for w in words:
            w_if = word_inv_frequency[w]
            for win_word in window:
                if win_word == w:
                    continue
                word_nb = word_neighbours.get(win_word)
                if not word_nb:
                    word_neighbours[win_word] = {w: w_if}
                else:
                    counter = word_nb.get(w, 0)
                    word_nb[w] = counter + w_if
            window.append(w)
        # we take x3 neighbours to filter out 2/3 by the
        # inverted frequency in _filter_neighbours_by_inv_f
        print('[*] neighbours_max = NEIGHBOURS_COUNT * 3')
        neighbours_max = NEIGHBOURS_COUNT * 3
        for key_word in word_neighbours:
            card = self.card_by_word[key_word]
            all_words = list(word_neighbours[key_word].items())
            all_words.sort(key=lambda wc: wc[1], reverse=True)
            card.neighbours = [word_index[v] for v, c in all_words[:neighbours_max]]
        self._filter_neighbours_by_inv_f()

    def _filter_neighbours_by_inv_f(self) -> None:
        """
        Leave up to <NEIGHBOURS_COUNT> from each word's neighbours list
        Sort the neighbours by inverted frequency:
        - the word has less chance to be left in the neighbours list,
          the more occurrences of this word are in all words' neighbour lists
        """
        print('[*] word_count: Dict[int, int] = {}')
        word_count: Dict[int, int] = {}
        neighbours = [set(c.neighbours) for c in self.cards]
        count_max = 0
        for i in range(len(self.cards)):
            count = 0
            for n_set in neighbours:
                if i in n_set:
                    count += 1
            word_count[i] = count
            count_max = max(count_max, count)
        print('[*] word_freq_penalty')
        word_freq_penalty = {w: c * NEIGHBOURS_COUNT * 0.7 / count_max
                             for w, c in word_count.items()}
        print('[*] for card in self.cards:')
        for card in self.cards:
            ordered_nb = []
            for i, w_index in enumerate(card.neighbours):
                order = i + word_freq_penalty[w_index]
                ordered_nb.append((w_index, order))
            ordered_nb.sort(key=lambda nb: nb[1])
            card.neighbours = [n for n, _ in ordered_nb[:NEIGHBOURS_COUNT]]

    def _find_synonyms(self):
        index_by_word = {self.cards[i].word: i for i in range(len(self.cards))}
        last_percent, counter, total = 0, 0, len(self.cards)
        for card in self.cards:
            snms = self.w2v_model.wv.most_similar(card.word, topn=SYNONYM_COUNT * 2)
            card.synonyms = [index_by_word[w] for w, _ in snms if w in index_by_word][:SYNONYM_COUNT]

            counter += 1
            percent = counter * 100 / total
            if percent - last_percent > 5:
                print(f"synonyms search: {round(percent)} % complete")
                last_percent = percent
