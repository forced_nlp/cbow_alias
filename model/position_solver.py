from typing import Dict, List, Optional

from model.constants import SIDE_A
from model.word_card import WordCard
from model.word_similarity_calculator import WordSimilarityCalculator


class PositionSolverSettings:
    def __init__(self,
                 sim_coeff_w: float = 1,
                 sim_coeff_h2ws: float = 0.75,
                 sim_coeff_h2wn: float = 0.6,
                 sim_coeff_rec: float = 0.95):
        self.sim_coeff_w = sim_coeff_w
        self.sim_coeff_h2ws = sim_coeff_h2ws
        self.sim_coeff_h2wn = sim_coeff_h2wn
        self.sim_coeff_rec = sim_coeff_rec

    def __repr__(self) -> str:
        return f'H2WS: {self.sim_coeff_h2ws}, H2WN: {self.sim_coeff_h2wn}, ' + \
               f'CREC: {self.sim_coeff_rec}'

    def __str__(self) -> str:
        return self.__repr__()


class PositionSolver:
    def __init__(self,
                 cards: List[WordCard],
                 word_to_card: Dict[str, WordCard],
                 settings: Optional[PositionSolverSettings] = None,
                 silent: bool = False):
        self.cards = cards
        self.word_to_card = word_to_card
        self.cards = list(self.word_to_card.values())
        self.side = ''
        self.pos_words: List[WordCard] = []
        self.hint: WordCard = None
        self.hint_count = 0
        self.calc = WordSimilarityCalculator(self.word_to_card)
        self.settings = settings or PositionSolverSettings()
        self.silent = silent

    def solve(self,
              words_to_solve: List[WordCard],
              hint: str,
              hint_count: int,
              side: str = SIDE_A) -> List[str]:
        self.pos_words = words_to_solve
        self.hint_count = hint_count
        self.side = side
        self.hint = self.word_to_card[hint]
        for w in self.pos_words:
            self.fill_word_coherence_list(self.hint, w)
        word_tags = self.calc.get_most_similar()
        w_t_str = '\n'.join([f'[{w}]: {t}' for w, t in word_tags])
        if not self.silent:
            print(w_t_str)
        return [w for w, _ in word_tags]

    def fill_word_coherence_list(self,
                                 hint: WordCard,
                                 word: WordCard,
                                 recursion_level: int = 1,
                                 coeff: float = 1) -> None:
        # similarity between word and hint
        self.calc.append(word.word,
                         word_a=hint.word, word_b=word.word,
                         tag=word.word, coeff=coeff)
        # .. between the hint and the word synonyms
        for s in word.synonyms:
            self.calc.append(word.word,
                             word_a=hint.word, word_b=self.cards[s].word,
                             tag=f'syn: {self.cards[s].word}',
                             coeff=coeff * self.settings.sim_coeff_h2ws)
        # .. between the hint and the word neighbours
        for s in word.neighbours:
            self.calc.append(word.word,
                             word_a=hint.word, word_b=self.cards[s].word,
                             tag=f'nbr: {self.cards[s].word}',
                             coeff=coeff * self.settings.sim_coeff_h2wn)

        if recursion_level:
            for sn_index in hint.synonyms:
                hint_sn = self.cards[sn_index]
                self.fill_word_coherence_list(
                    hint_sn, word, recursion_level-1, coeff * self.settings.sim_coeff_rec)
