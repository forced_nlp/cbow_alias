import numpy as np
from unittest import TestCase

from sklearn.metrics.pairwise import cosine_similarity

from model.dictionary_file_manager import DictionaryFileManager
from model.position import Position
from model.position_solver import PositionSolver


class TestPositionSolver(TestCase):
    @classmethod
    def setUpClass(cls):
        mgr = DictionaryFileManager()
        cls.dc = mgr.load('en')
        cls.word_card = {w.word: w for w in cls.dc.words}

    def test_calc_similarity(self):
        word_a, word_b = 'paragraph', 'cause'
        card_a, card_b = self.word_card[word_a], self.word_card[word_b]
        m = cosine_similarity(np.array([card_a.vector]), np.array([card_b.vector]))
        print(m)

    def test_solve(self):
        pos = self._setup_position_73()
        solver = PositionSolver(
            self.dc.words,
            self.word_card
        )
        solution = solver.solve(pos.words_to_solve, 'nobleman', 3)
        print(f'hint: {solver.hint.word}, [' +
              ', '.join(solution) + ']')

    def _setup_position_403(self) -> Position:
        pos = Position()
        pos.words_a = [self.word_card[w] for w in [
            'volume', 'prison', 'thunder', 'universe', 'luna'
        ]]
        pos.words_b = [self.word_card[w] for w in [
            'oblivion', 'loaf', 'rainbow'
        ]]
        pos.words_mine = [self.word_card['crusade']]
        return pos

    def _setup_position_73(self) -> Position:
        pos = Position()
        pos.words_a = [self.word_card[w] for w in [
            'volume', 'leaf', 'verse', 'fork', 'luna', 'queen', 'knight'
        ]]
        pos.words_b = [self.word_card[w] for w in [
            'oblivion', 'loaf', 'empire'
        ]]
        pos.words_mine = [self.word_card['crusade']]
        return pos

