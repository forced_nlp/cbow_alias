import os
from typing import List, Dict
from unittest import TestCase
import numpy as np
from sklearn.metrics.pairwise import cosine_distances

from commands.paths import TOKENIZED_CORPUS_PATH
from model.dictionary_builder import DictionaryBuilder
from model.dictionary_file_manager import DictionaryFileManager
from model.word_card import WordCard


class TestDictionaryBuilder(TestCase):
    def test_build_en(self):
        corpus_file_path = os.path.join(TOKENIZED_CORPUS_PATH, 'en', 'tokenized_corpus.txt')
        lemmas_file_path = os.path.join(TOKENIZED_CORPUS_PATH, 'en', 'lemmatized_corpus.txt')

        builder = DictionaryBuilder()
        dc = builder.build('en', corpus_file_path, lemmas_file_path)

        mgr = DictionaryFileManager()
        mgr.save(dc)

    def test_load_dict(self):
        mgr = DictionaryFileManager()
        dc = mgr.load('en')
        assert dc.words
        word_to_card = {w.word: w for w in dc.words}

        # test_words = ['empire', 'loss', 'volume', 'queen']
        test_words = ['knee', 'tenderness', 'daisy', 'volume', 'prison']
        for wrd in test_words:
            word: WordCard = word_to_card[wrd]
            neighbours = [dc.words[ni].word for ni in word.neighbours]
            synonyms = [dc.words[ni].word for ni in word.synonyms]
            print(f'"{wrd}" synonyms are: {synonyms},\n'
                  f'neighbours are {neighbours}' + '\n\n')

        # test that "synonyms'" vectors are closer than average ones
        avg_dists = []
        syn_dists = []
        for i in range(len(test_words)):
            word: WordCard = word_to_card[test_words[i]]
            synonyms = [dc.words[ni].word for ni in word.synonyms]
            for sn in synonyms:
                syn_dists.append(self.get_word_distance(word_to_card, test_words[i], sn))

            for j in range(i + 1, len(test_words)):
                avg_dists.append(self.get_word_distance(word_to_card, test_words[i], test_words[j]))

        print(f'Min avg dist: {min(avg_dists)}')
        print(f'Min syn dist: {min(syn_dists)}')

    def test_word_math(self):
        mgr = DictionaryFileManager()
        dc = mgr.load('en')
        word_to_card = {w.word: w for w in dc.words}

        # word_keys = ['enemy', 'queen', 'escape', 'daily', 'bouillon', 'always']
        d1 = self.get_word_distance(word_to_card, 'empire', 'liberty')  # short
        d2 = self.get_word_distance(word_to_card, 'empire', 'stock')  # short
        d3 = self.get_word_distance(word_to_card, 'volume', 'empire')  # ?
        d4 = self.get_word_distance(word_to_card, 'volume', 'hawk')  # short
        d5 = self.get_word_distance(word_to_card, 'queen', 'age')  # short
        d6 = self.get_word_distance(word_to_card, 'queen', 'purpose')  # ?
        assert d3 > d4
        assert d6 > d5
        assert d2 > d1

    def test_cosine_sim(self):
        # cosine_similarity: may not be the best option!
        a = np.array([
            [0.1, 0.5, 0.3], [0.4, 0.0, 0.0], [0.2, 0.2, 0.2]])
        b = np.array([
            [0.5, 0.0, 0.5], [0.4, 0.4, 0.4], [0.1, 0.3, 0.2]])
        similarities: np.ndarray = cosine_distances(a, b)
        assert(similarities)

    @classmethod
    def get_cosine_distance(cls, a: List[float], b: List[float]):
        d = cosine_distances(np.array([a]), np.array([b]))
        return d[0][0]

    def get_word_distance(self,
                          word_to_card: Dict[str, WordCard],
                          word_a: str,
                          word_b: str) -> float:
        va, vb = word_to_card[word_a].vector, word_to_card[word_b].vector
        return self.get_cosine_distance(va, vb)
