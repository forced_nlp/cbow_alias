import codecs
import os
from typing import List, Set, Tuple
from unittest import TestCase

from model.dictionary_file_manager import DictionaryFileManager
from model.position_solver import PositionSolver, PositionSolverSettings


class OptimizationSample:
    def __init__(self,
                 words: List[str],
                 hint: str,
                 count: int,
                 correct_values: Set[str]):
        self.words = words
        self.hint = hint
        self.count = count
        self.correct_values = correct_values
        self.score = 0


class TestOptimizePositionSolver(TestCase):
    @classmethod
    def setUpClass(cls):
        mgr = DictionaryFileManager()
        cls.dc = mgr.load('en')
        cls.word_card = {w.word: w for w in cls.dc.words}

    def test_optimize(self):
        samples = self.load_samples()

        settings_vector = []
        for i in range(10):
            sim_coeff_h2ws = 1 - 0.05 * i
            for j in range(10):
                sim_coeff_h2wn = 1 - 0.05 * j
                for k in range(10):
                    sim_coeff_rec = 1 - 0.05 * k
                    settings_vector.append(PositionSolverSettings(
                        sim_coeff_h2ws=sim_coeff_h2ws,
                        sim_coeff_h2wn=sim_coeff_h2wn,
                        sim_coeff_rec=sim_coeff_rec))

        best_score, sum_scores, max_possible_score = -(10**10), 0, 0
        best_vector = PositionSolverSettings(
            sim_coeff_h2ws=0.7, sim_coeff_h2wn=0.6, sim_coeff_rec=0.8)
        last_percent = 0
        for i in range(len(settings_vector)):
            score, max_score = self.calculate_score_on_set(samples, settings_vector[i])
            max_possible_score = max_score
            sum_scores += score

            if score > best_score:
                print(f'Score is updated: {best_score} -> {score}')
                best_score = score
                best_vector = settings_vector[i]
            percent = i * 100 / len(settings_vector)
            if (percent - last_percent) > 5:
                last_percent = percent
                print(f'{round(percent)} % completed')

        avg_score = sum_scores / len(settings_vector)
        print(f'Completed\nBest vector: {best_vector}\nBest score: '
              f'{best_score} (of possible {max_possible_score})\n'
              f'Average score: {avg_score:.1f}')

    def calculate_score_on_set(self,
                               samples: List[OptimizationSample],
                               settings: PositionSolverSettings) -> Tuple[int, int]:
        score, max_score = 0, 0
        for sample in samples:
            s, ms = self.calculate_score(sample, settings)
            score += s
            max_score += ms
        return score, max_score

    def calculate_score(self,
                        sample: OptimizationSample,
                        settings: PositionSolverSettings) -> Tuple[int, int]:
        solver = PositionSolver(
            self.dc.words,
            self.word_card,
            settings=settings,
            silent=True
        )
        solved = set(solver.solve(
            [self.word_card[w] for w in sample.words],
            sample.hint,
            len(sample.correct_values))[:len(sample.correct_values)])
        tp = sum([1 for w in sample.correct_values if w in solved])
        fp = sum([1 for w in solved if w not in sample.correct_values])
        sample.score = tp - fp
        return sample.score, len(sample.correct_values)

    def load_samples(self) -> List[OptimizationSample]:
        log_path = os.path.dirname(os.path.realpath(__file__))
        log_path = os.path.join(log_path, 'guessing_optimize.csv')
        with codecs.open(log_path, 'r', encoding='utf-8') as fr:
            lines = fr.readlines()

        samples = []
        for line in lines[1:]:
            parts = line.split(',')
            sample = OptimizationSample(
                words=[wp for wp in parts[0].split(';') if wp],
                hint=parts[1],
                count=int(parts[2]),
                correct_values={wp for wp in parts[3].split(';') if wp},
            )
            samples.append(sample)
        return samples
