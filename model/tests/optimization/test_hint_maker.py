import datetime

from unittest import TestCase

from game.game_basics import GameBasics
from model.dictionary_file_manager import DictionaryFileManager
from model.hint_maker import HintMaker
from model.position import Position


class TestHintMaker(TestCase):
    @classmethod
    def setUpClass(cls):
        mgr = DictionaryFileManager()
        cls.dc = mgr.load('en')
        cls.word_card = {w.word: w for w in cls.dc.words}

    def test_make_hint(self):
        pos = self._setup_position()
        maker = HintMaker(
            self.dc.words,
            self.word_card
        )
        start = datetime.datetime.now()
        hint = maker.give_hint(pos)
        elapsed = (datetime.datetime.now() - start).total_seconds()
        print(f'{hint}')
        print(f'Time taken: {elapsed} s')

    def test_time_taken_by_hinting(self):
        gm = GameBasics(self.dc)
        gm.init_position(10)
        pos = gm.pos

        maker = HintMaker(
            self.dc.words,
            self.word_card
        )
        start = datetime.datetime.now()
        hint = maker.give_hint(pos)
        elapsed = (datetime.datetime.now() - start).total_seconds()
        print(f'{hint}')
        print(f'Time taken: {elapsed} s')

    def _setup_position(self) -> Position:
        pos = Position()
        pos.words_a = [self.word_card[w] for w in [
            'volume', 'leaf', 'verse', 'fork', 'luna', 'queen', 'knight'
        ]]
        pos.words_b = [self.word_card[w] for w in [
            'oblivion', 'loaf', 'empire'
        ]]
        pos.words_mine = [self.word_card['crusade']]
        return pos

