from unittest import TestCase

from model.corpus_math import CorpusMath


class TestCorpusMath(TestCase):
    def test_sum(self):
        formula = 'man + power'
        result = CorpusMath.calculate_words(formula, 3)
        print(f"{formula} = {result}")

    def test_sub(self):
        formula = 'pig - woman + ear'
        result = CorpusMath.calculate_words(formula, 5)
        print(f"{formula} = {result}")

    def test_sub_sum(self):
        formula = 'queen - woman + gold'
        result = CorpusMath.calculate_words(formula, 5)
        print(f"{formula} = {result}")
