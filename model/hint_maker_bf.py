import numpy as np
from typing import List, Dict, Optional, Tuple, Any, Set

from sklearn.metrics.pairwise import cosine_similarity

from model.constants import SIDE_A
from model.hint_maker import GuessMakerSettings, WordHint
from model.position import Position
from model.word_card import WordCard


class HintMakerBf:
    def __init__(self,
                 cards: List[WordCard],
                 word_to_card: Dict[str, WordCard],
                 # settings: Optional[PositionSolverSettings] = None,
                 silent: bool = False,
                 settings: Optional[GuessMakerSettings] = None):
        # super().__init__(cards, word_to_card, settings, True)
        self.cards = cards
        self.word_to_card = word_to_card
        self.cards = list(self.word_to_card.values())
        self.side = SIDE_A
        self.pos: Position = None
        self.settings = settings or GuessMakerSettings()
        self.silent = silent

    def give_hint(self,
                  pos: Position,
                  side: str = SIDE_A) -> WordHint:
        self.pos = pos
        self.side = side

        indexes = {self.cards[i].word: i for i in range(len(self.cards))}

        hints: Dict[int, Dict[str, float]] = {}

        word_to_index: Dict[int, Dict[str, float]] = {}

        for w in pos.words_a:
            word_to_index[indexes[w.word]] = {w.word: 1}
            for ind in w.neighbours[:4] + w.synonyms[:6]:
                if ind in word_to_index:
                    word_to_index[ind][w.word] = self.settings.synonym_coeff
                else:
                    word_to_index[ind] = {w.word: self.settings.synonym_coeff}

        w2 = [(self.cards[ind], w) for ind, w in word_to_index.items()]
        w2.sort(key=lambda w: len(w[1]), reverse=True)

        def union_hinted_words(
                hinted_words: Dict[str, float],
                new_hints: Dict[str, float]):
            for w, c in new_hints.items():
                if w in hinted_words:
                    hinted_words[w] = max(c, hinted_words[w])
                else:
                    hinted_words[w] = c

        pos_words = set(self.pos.all_words)
        for i in range(len(self.cards)):
            hinted_words: Dict[str, float] = {}
            c = self.cards[i]
            if c.word in pos_words:
                continue
            w2i = word_to_index.get(i)
            if w2i:
                union_hinted_words(hinted_words, w2i)
            for ind in c.neighbours[:4]:
                w2i = word_to_index.get(ind)
                if w2i:
                    union_hinted_words(hinted_words, w2i)
            for ind in c.synonyms[:6]:
                w2i = word_to_index.get(ind)
                if w2i:
                    union_hinted_words(hinted_words, w2i)
            # if len(hinted_words) > 1:
            hints[i] = hinted_words

        # get the longest from hints
        hint_dic_list: List[Tuple[int, Dict[str, float]]] = [(ind, words) for ind, words in hints.items()]
        hint_list: List[Tuple[int, List[Tuple[str, float]], float]] = []
        for i, words in hint_dic_list:
            word_list: List[Tuple[str, float]] = list(words.items())
            word_list.sort(key=lambda itm: itm[1], reverse=True)
            word_list = word_list[:3]
            score = sum([c*c for _, c in word_list])
            hint_list.append((i, word_list, score))


        hint_list.sort(key=lambda h: h[2], reverse=True)
        longest = hint_list[0]
        hint = WordHint(self.cards[longest[0]].word)
        hint.hint_score = [(w, 1) for w in longest[1]]
        return hint
