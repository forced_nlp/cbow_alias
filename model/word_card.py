from dataclasses import dataclass
from typing import List, Any, Dict

from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class WordCard:
    word: str = None
    pos: str = None
    frequency: float = 0
    frequency_rank: int = 0
    frequency_rel_rank: float = 0
    vector: List[float] = None
    neighbours: List[int] = None
    synonyms: List[int] = None

    def __str__(self):
        return f'{self.word} ({self.pos}), f={self.frequency:.3f}, rank={self.frequency_rank}'

    def __repr__(self):
        return str(self)

    def to_dict(self) -> Dict[str, Any]:
        # override dataclass_json.to_dict() because it's too slow
        return {'word': self.word, 'pos': self.pos,
                'frequency': self.frequency, 'frequency_rank': self.frequency_rank,
                'frequency_rel_rank': self.frequency_rel_rank, 'vector': self.vector,
                'neighbours': self.neighbours, 'synonyms': self.synonyms}
