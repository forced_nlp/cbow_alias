from typing import List
import numpy as np
import re
from sklearn.metrics.pairwise import cosine_similarity

from model.dictionary_file_manager import DictionaryFileManager
from model.word_card import WordCard


mgr = DictionaryFileManager()
DICTIONARY = mgr.load('en')


class CorpusMath:
    @classmethod
    def calculate_words(cls, formula: str, result_max: int = 3) -> List[str]:
        words = [w.group().strip() for w in re.finditer(r'[^-+]+', formula)]
        if not words:
            return []

        card_by_word = {w.word: w for w in DICTIONARY.words}
        cards: List[WordCard] = [card_by_word[w] for w in words]

        ops = [f for f in formula if f in {'+', '-'}]

        vector = cards[0].vector
        for card in cards[1:]:
            op = ops[0]
            del ops[0]
            sign = -1 if op == '-' else 1
            vector = [a_i + sign * b_i for a_i, b_i in zip(vector, card.vector)]

        return cls._find_nearest_words(vector, result_max)

    @classmethod
    def _find_nearest_words(cls, vector: List[float], result_max: int) -> List[str]:
        vectors = [w.vector for w in DICTIONARY.words]
        m = cosine_similarity(np.array([vector]), np.array(vectors))
        index_val = [(i, m[0][i]) for i in range(len(m[0]))]
        index_val.sort(key=lambda iv: iv[1], reverse=True)
        indices = [iv[0] for iv in index_val[:result_max]]
        return [DICTIONARY.words[i].word for i in indices]
