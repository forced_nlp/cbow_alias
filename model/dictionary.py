from typing import List

from model.word_card import WordCard


class Dictionary:
    def __init__(self, lang_code: str):
        self.lang_code = lang_code
        self.words: List[WordCard] = []
