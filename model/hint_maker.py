import numpy as np
from typing import List, Dict, Optional, Tuple, Any

from sklearn.metrics.pairwise import cosine_similarity

from model.constants import SIDE_A
from model.position import Position
from model.word_card import WordCard


class WordHint:
    def __init__(self,
                 word: str):
        self.word = word
        self.hint_score: List[Tuple[str, float]] = []

    def __repr__(self) -> str:
        scores = ', '.join([f'{h}: {s:.3f}' for h, s in self.hint_score])
        return f'{self.word} x{len(self.hint_score)} [{scores}]'

    @property
    def count(self) -> int:
        return len(self.hint_score)


class WordTag:
    def __init__(self,
                 word: str,
                 hint: str,
                 side: str,
                 score_points: float,
                 multiplier: float):
        self.word = word
        self.hint = hint
        self.side = side
        self.score_points = score_points
        self.multiplier = multiplier

    def __repr__(self) -> str:
        return f'{self.word} -> {self.hint} ({self.side})'

    def __str__(self) -> str:
        return self.__repr__()


class GuessMakerSettings:
    def __init__(self):
        self.score_by_side = {
            'a': 1,
            'b': -5,
            'n': -2,
            'm': -999
        }
        self.synonym_coeff = 0.86
        self.neighbour_coeff = 0.72
        self.threshold_power = 1.2
        self.threshold_value = 0.65
        self.threshold_multiplier = 1 / self.threshold_value
        self.recursion_coeff = 0.8

    def threshold_func(self, sim: Any) -> Any:
        y = (self.threshold_multiplier *
                (sim - self.threshold_value))**self.threshold_power \
            if sim > self.threshold_value else 0
        return y


class HintMaker:
    def __init__(self,
                 cards: List[WordCard],
                 word_to_card: Dict[str, WordCard],
                 # settings: Optional[PositionSolverSettings] = None,
                 silent: bool = False,
                 settings: Optional[GuessMakerSettings] = None):
        # super().__init__(cards, word_to_card, settings, True)
        self.cards = cards
        self.word_to_card = word_to_card
        self.cards = list(self.word_to_card.values())
        self.side = SIDE_A
        self.pos: Position = None
        self.settings = settings or GuessMakerSettings()
        self.silent = silent

    def give_hint(self,
                  pos: Position,
                  side: str = SIDE_A) -> WordHint:
        self.pos = pos
        self.side = side

        tags = self._make_all_tags()
        tag_vectors = [self.word_to_card[t.hint].vector for t in tags]

        pos_words = set(self.pos.all_words)
        # TODO: check for stem etc
        hint_options = [c for c in self.cards if c.word not in pos_words]
        hint_option_vectors = [opt.vector for opt in hint_options]
        # TODO: multiple columns by inverted frequency of the hints
        # Should I? I already applied "i.f." to the neighbours list

        m = cosine_similarity(np.array(tag_vectors), np.array(hint_option_vectors))

        # multiply rows by coeffs: 1 for the word itself, SYNONYM_COEFF for the word's synonym
        c = np.array([t.multiplier for t in tags])
        m = m * c[:, np.newaxis]

        # get the matrix of maximums per word across tags axis
        tag_start_stop = self._group_tags_by_words(tags)
        sim_matrix = []
        for tag, start, stop in tag_start_stop:
            row = np.max(m[list(range(start, stop)), :], axis=0)
            sim_matrix.append(row)

        # apply threshold to values
        sim_nm = np.array(sim_matrix)
        t_func = np.vectorize(self.settings.threshold_func, otypes=[np.float])
        sim_nm = t_func(sim_nm)

        # apply coeffs to rows: side 'a' coeff, ... 'b' ... 'mine'
        tags = [t for t, _s, _f in tag_start_stop]
        c = np.array([t.score_points for t in tags])
        sim_nm = sim_nm * c[:, np.newaxis]

        # get a column with max sum
        return self._select_hint(sim_nm, tags, hint_options)

    def _select_hint(self,
                     sim_m: np.ndarray,
                     tags: List[WordTag],
                     hint_options: List[WordCard]) -> WordHint:
        # get a column with max sum
        sum_scores = np.sum(sim_m, axis=0)
        index = sum_scores.argmax()
        hint = WordHint(hint_options[index].word)

        column = sim_m[:, [index]]
        for i in range(len(column)):
            if column[i] > 0:
                hint.hint_score.append((tags[i].word, column[i][0]))
        return hint

    def _make_all_tags(self) -> List[WordTag]:
        tags: List[WordTag] = []
        for w in self.pos.words_a:
            tags += self._make_tags(w, 'a')
        for w in self.pos.words_b:
            tags += self._make_tags(w, 'b')
        for w in self.pos.words_n:
            tags += self._make_tags(w, 'n')
        for w in self.pos.words_mine:
            tags += self._make_tags(w, 'm')
        return tags

    def _group_tags_by_words(self, tags: List[WordTag]) -> List[Tuple[WordTag, int, int]]:
        tag_start_stop: List[Tuple[WordTag, int, int]] = []
        cur_tag: Optional[WordTag] = None
        start = 0
        for i in range(len(tags)):
            if not cur_tag or tags[i].word != cur_tag.word:
                if cur_tag:
                    tag_start_stop.append((cur_tag, start, i))
                start = i
                cur_tag = tags[i]
        tag_start_stop.append((cur_tag, start, len(tags)))
        return tag_start_stop

    def _make_tags(self, word: WordCard, side: str) -> List[WordTag]:
        score_points = self.settings.score_by_side[side]
        tags: List[WordTag] = [WordTag(word.word, word.word, side, score_points, 1)]
        for ind in word.synonyms:
            tags.append(WordTag(word.word, self.cards[ind].word, side,
                                score_points, self.settings.synonym_coeff))
        for ind in word.neighbours:
            tags.append(WordTag(word.word, self.cards[ind].word, side,
                                score_points, self.settings.neighbour_coeff))
        return tags
